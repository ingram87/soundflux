--
-- VHDL Architecture soundflux_lib.BUFFER_CONTROL.behav
--
-- Created:
--          by - marer661.student (egypten-08.edu.isy.liu.se)
--          at - 13:27:05 09/25/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE ieee.numeric_std.all;

ENTITY SRAM_CONTROL IS
   PORT( 
      fpga_clk_65M      : IN     std_logic;
      reset_n           : IN     std_logic;
      sram_ce_n         : OUT    std_logic;
      sram_lb_n         : OUT    std_logic;
      sram_oe_n         : OUT    std_logic;
      sram_ub_n         : OUT    std_logic;
      sram_we_n         : OUT    std_logic;
      data_out_graphics : OUT    std_logic_vector (15 DOWNTO 0);
      data_out_sound    : OUT    std_logic_vector (15 DOWNTO 0);
      r_vga             : IN     std_logic;
      rw_sound          : IN     RW_TYPE;
      sram_address      : OUT    std_logic_vector (19 DOWNTO 0);
      data_in           : IN     std_logic_vector (15 DOWNTO 0);
      data_out          : OUT    std_logic_vector (15 DOWNTO 0);
      data_in_sound     : IN     std_logic_vector (15 DOWNTO 0);
      delay             : IN     std_logic_vector (10 DOWNTO 0)
   );

-- Declarations

END SRAM_CONTROL ;

--
ARCHITECTURE behav OF SRAM_CONTROL IS
    -- Internal types
    TYPE data_dir_type IS (IDLE, SOUND, GRAPHICS);
    TYPE buffer_state_type IS (NORMAL, RESIZED); 
    --TYPE sram_memory_space IS RANGE 0 TO 4096;  
    
    SIGNAL data_dir : data_dir_type;
    SIGNAL buffer_state : buffer_state_type;
    
    -- Internal signals
    CONSTANT start_of_sram_memory   : NATURAL := 0;
    CONSTANT start_of_buffer_space  : NATURAL := 0;
    CONSTANT end_of_sram_memory     : NATURAL := 970200;
    CONSTANT sample_frequency       : NATURAL := 88200;
  
  BEGIN -- Architecture
    
    output_demux : PROCESS(data_in, data_dir) -- Demux for output data. 
    BEGIN
      CASE data_dir IS
      WHEN IDLE => data_out_sound <= (others => '0');
      WHEN SOUND => 
        CASE buffer_state IS
        WHEN NORMAL => data_out_sound <= data_in;
        WHEN RESIZED => data_out_sound <= (others => '0');
        END CASE;
      WHEN GRAPHICS => data_out_graphics <= data_in;
      END CASE;
    END PROCESS;
    
    control : PROCESS(fpga_clk_65M, reset_n)
      -- Local variables
      VARIABLE rw_sound_old : RW_TYPE;
      VARIABLE delay_old : std_logic_vector(11 DOWNTO 0);
      VARIABLE start_of_buffer : NATURAL; -- Insert sob address.
      VARIABLE end_of_buffer : NATURAL;
    
      -- Procedure to set the sram mode of operation.
      PROCEDURE set_sram_mode(mode : RW_TYPE) IS
      BEGIN
      IF mode = IDLE THEN
        -- Set SRAM in idle
        sram_ce_n <= '0';
        sram_oe_n <= '1';
        sram_we_n <= '1';
      ELSIF mode = READ THEN
        -- Set SRAM in read mode
        sram_oe_n <= '0';
        sram_we_n <= '1';
      ELSIF mode = WRITE THEN
        -- Set SRAM in write mode
        sram_oe_n <= '1';
        sram_we_n <= '0';
      END IF;
      END PROCEDURE;
      
      PROCEDURE reset_module IS
      BEGIN    
        -- Initial setup
        sram_ce_n <= '0';
        sram_lb_n <= '0';
        sram_ub_n <= '0';
        -- Set output disabled
        set_sram_mode(IDLE);
        
        -- Assume delay of 0
        start_of_buffer := start_of_buffer_space;    
        end_of_buffer := start_of_buffer_space;
        buffer_state <= NORMAL;
      END PROCEDURE;
      
      -- Procedure to increment buffer pointers
      PROCEDURE move_buffer IS
      BEGIN
        IF not (unsigned(delay) = 0) THEN
          IF end_of_buffer = end_of_sram_memory THEN
            
            -- End pointer at end of SRAM
            start_of_buffer := start_of_buffer + 1;
            end_of_buffer := start_of_buffer_space;
            
          ELSIF start_of_buffer = end_of_sram_memory THEN
            
            -- Start buffer at end of SRAM
            start_of_buffer := start_of_buffer_space;
            end_of_buffer := end_of_buffer + 1;
            
          ELSE
            
            -- Increment both pointers          
            start_of_buffer := start_of_buffer + 1;
            end_of_buffer := end_of_buffer + 1;
            
          END IF;
          
          IF start_of_buffer = unsigned(delay) THEN
            -- Revert to normal buffer state
            buffer_state <= NORMAL;
          END IF;
          
        END IF;
      END PROCEDURE;
      
    BEGIN -- Process control
      
      IF reset_n = '0' THEN
        
        -- Asyncronous reset
        reset_module;
        
      ELSIF rising_edge(fpga_clk_65M) THEN
        
        IF not (delay = delay_old) THEN
          -- New delay value, resize and reset buffer
          start_of_buffer := start_of_buffer_space;
          end_of_buffer := start_of_buffer_space + to_integer(unsigned(delay)) * sample_frequency - 1; -- Translated delay to memory addresses
          
          -- Enter RESIZED buffer state
          buffer_state <= RESIZED; 
          
          -- Update old delay
          delay_old := delay;         
          
        ELSIF not (rw_sound = rw_sound_old) THEN
          
          -- Sound module event
          CASE rw_sound IS
          WHEN IDLE =>
            -- Set idle mode
            set_sram_mode(IDLE);
          WHEN READ =>
            -- Set read mode and fetch the
            -- first value from the buffer.
            set_sram_mode(READ);
            data_dir <= SOUND;
            sram_address <= std_logic_vector(to_unsigned(start_of_buffer, sram_address'length));            
          WHEN WRITE =>
            -- Set write mode. Append the data provided on
            -- data_in_sound to the buffer and move the pointers.
            set_sram_mode(WRITE);
            data_dir <= IDLE;
            move_buffer;         
            sram_address <= std_logic_vector(to_unsigned(end_of_buffer, sram_address'length));
            data_out <= data_in_sound;
            
          END CASE;
          
        --ELSIF R_VGA'EVENT THEN
          
          -- Graphics module event
          --CASE r_vga IS
--          WHEN IDLE =>
--            -- Set idle mode
--            set_sram_mode(IDLE);
--          WHEN READ =>
--            -- Set read mode. Fetch pixel
--            -- data from the memory.
--            set_sram_mode(READ);
--          WHEN WRITE =>
--            -- Not applicable
--            set_sram_mode(IDLE);
--          END CASE;
          
        END IF;
        
        rw_sound_old := rw_sound;
        
      END IF;
      
    END PROCESS;    
  END ARCHITECTURE behav;
  
  
  
