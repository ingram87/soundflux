--
-- VHDL Architecture soundflux_lib.control.behav
--
-- Created:
--          by - danle157.student (olympen-14.edu.isy.liu.se)
--          at - 08:58:06 09/27/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY control IS
--   GENERIC( 
--      vol_reset       : unsigned(3 downto 0) := "0101";
--      balance_reset   : unsigned(3 downto 0) := "0101";
--      eco_vol_reset   : unsigned(3 downto 0) := "0101";
--      eco_delay_reset : unsigned(3 downto 0) := "0101";
--      vol_max         : unsigned(3 downto 0) := "1111";
--      vol_min         : unsigned(3 downto 0) := "0000";
--      balance_max     : unsigned(3 downto 0) := "1111";
--      balance_min     : unsigned(3 downto 0) := "0000";
--      eco_vol_max     : unsigned(3 downto 0) := "1111";
--      eco_vol_min     : unsigned(3 downto 0) := "0000";
--      eco_delay_max   : unsigned(3 downto 0) := "1111";
--      eco_delay_min   : unsigned(3 downto 0) := "0000"
--   );
   PORT( 
      fpga_clk_65M    : IN     std_logic;
      reset_n         : IN     std_logic;
      scancode        : IN     std_logic_vector (7 DOWNTO 0);
      scancode_stable : IN     std_logic;
      balance         : OUT    unsigned (3 DOWNTO 0);
      eco_delay       : OUT    unsigned (3 DOWNTO 0);
      eco_vol         : OUT    unsigned (3 DOWNTO 0);
      vol             : OUT    unsigned (3 DOWNTO 0)
   );

-- Declarations

END control ;

--
ARCHITECTURE behav OF control IS
  CONSTANT break_code : std_logic_vector(7 downto 0) := "11110000";
  CONSTANT w_key_code : std_logic_vector(7 downto 0) := "00011101";
  CONSTANT s_key_code : std_logic_vector(7 downto 0) := "00011011";
  CONSTANT a_key_code : std_logic_vector(7 downto 0) := "00011100";
  CONSTANT d_key_code : std_logic_vector(7 downto 0) := "00100011";
  CONSTANT r_key_code : std_logic_vector(7 downto 0) := "00101101";
  CONSTANT f_key_code : std_logic_vector(7 downto 0) := "00101011";
  CONSTANT t_key_code : std_logic_vector(7 downto 0) := "00101100";
  CONSTANT g_key_code : std_logic_vector(7 downto 0) := "00110100";
  
   CONSTANT    vol_reset       : unsigned(3 downto 0) := "0101";
   CONSTANT   balance_reset   : unsigned(3 downto 0) := "0101";
   CONSTANT   eco_vol_reset   : unsigned(3 downto 0) := "0101";
    CONSTANT  eco_delay_reset : unsigned(3 downto 0) := "0101";
    CONSTANT  vol_max         : unsigned(3 downto 0) := "1111";
    CONSTANT  vol_min         : unsigned(3 downto 0) := "0000";
    CONSTANT  balance_max     : unsigned(3 downto 0) := "1111";
    CONSTANT  balance_min     : unsigned(3 downto 0) := "0000";
    CONSTANT  eco_vol_max     : unsigned(3 downto 0) := "1111";
    CONSTANT  eco_vol_min     : unsigned(3 downto 0) := "0000";
    CONSTANT  eco_delay_max   : unsigned(3 downto 0) := "1111";
    CONSTANT  eco_delay_min   : unsigned(3 downto 0) := "0000";
  
  SIGNAL previous_scancode_stable : std_logic;
  SIGNAL previous_was_break : std_logic;
BEGIN
  set_command: PROCESS(fpga_clk_65M, reset_n)
  BEGIN
    IF rising_edge(fpga_clk_65M) THEN
      -- Only execute command on rising edge of scancode_stable.
      IF previous_scancode_stable = '0' and scancode_stable = '1' THEN
        -- Choose command depending on wich scancode is available.
        -- If scancode is break_code, set a flag indicating we
        -- should ignore the next scancode.
        IF scancode = break_code THEN
          previous_was_break <= '1';
        ELSIF previous_was_break = '1' THEN
          previous_was_break <= '0';
        ELSE
          previous_was_break <= '0';
          IF scancode = w_key_code and vol < vol_max THEN
            vol <= vol + 1;
          ELSIF scancode = s_key_code and vol > vol_min THEN
            vol <= vol - 1;
          ELSIF scancode = a_key_code and balance > balance_min THEN
            balance <= balance - 1;
          ELSIF scancode = d_key_code and balance < balance_max THEN
            balance <= balance + 1;
          ELSIF scancode = r_key_code and eco_vol < eco_vol_max THEN
            eco_vol <= eco_vol + 1;
          ELSIF scancode = f_key_code and eco_vol > eco_vol_min THEN
            eco_vol <= eco_vol - 1;
          ELSIF scancode = t_key_code and eco_delay < eco_delay_max THEN
            eco_delay <= eco_delay + 1;
          ELSIF scancode = g_key_code and eco_delay > eco_delay_min THEN
            eco_delay <= eco_delay - 1;
          END IF;
        END IF;
      END IF;
      
      previous_scancode_stable <= scancode_stable;
    END IF;
    
    --Async reset
    IF reset_n = '0' THEN
      vol <= vol_reset;
      balance <= balance_reset;
      eco_vol <= eco_vol_reset;
      eco_delay <= eco_delay_reset;
    END IF;
  END PROCESS;
END ARCHITECTURE behav;

