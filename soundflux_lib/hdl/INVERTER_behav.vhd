--
-- VHDL Architecture soundflux_lib.INVERTER.behav
--
-- Created:
--          by - marer661.student (egypten-05.edu.isy.liu.se)
--          at - 13:46:20 09/24/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;

ENTITY INVERTER IS
   PORT( 
      fpga_reset_n : IN     std_logic;
      areset       : OUT    STD_LOGIC
   );

-- Declarations

END INVERTER ;

--
ARCHITECTURE behav OF INVERTER IS
BEGIN
  areset <= not fpga_reset_n;
END ARCHITECTURE behav;

