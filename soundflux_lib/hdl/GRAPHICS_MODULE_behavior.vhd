--
-- VHDL Architecture soundflux_lib.GRAPHICS_MODULE.behavior
--
-- Created:
--          by - rolst191.student (olympen-12.edu.isy.liu.se)
--          at - 14:22:13 09/27/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;

ENTITY GRAPHICS_MODULE IS
   PORT( 
      balance           : IN     unsigned (3 DOWNTO 0);
      data_out_graphics : IN     std_logic_vector (15 DOWNTO 0);
      eco_delay         : IN     unsigned (3 DOWNTO 0);
      eco_vol           : IN     unsigned (3 DOWNTO 0);
      fpga_clk_65M      : IN     std_logic;
      reset_n           : IN     std_logic;
      vol               : IN     unsigned (3 DOWNTO 0);
      r_vga             : OUT    std_logic;
      vga_b             : OUT    std_logic_vector (7 DOWNTO 0);
      vga_blank_n       : OUT    std_logic;
      vga_clk           : OUT    std_logic;
      vga_g             : OUT    std_logic_vector (7 DOWNTO 0);
      vga_hsync_n       : OUT    std_logic;
      vga_r             : OUT    std_logic_vector (7 DOWNTO 0);
      vga_sync          : OUT    std_logic;
      vga_vsync_n       : OUT    std_logic
   );

-- Declarations

END GRAPHICS_MODULE ;

--
ARCHITECTURE behavior OF GRAPHICS_MODULE IS
  SIGNAL vga_vsync_n1 : std_logic;
  SIGNAL vga_blank_n1 : std_logic;
  SIGNAL vga_hsync_n1 : std_logic;
  SIGNAL vga_vsync_n2 : std_logic;
  SIGNAL vga_blank_n2 : std_logic;
  SIGNAL vga_hsync_n2 : std_logic;
BEGIN
  vga_clk <= NOT fpga_clk_65M;
  vga_sync <= '0';
  pipeline: PROCESS(clk)
  BEGIN
    IF(rising_edge(clk)) THEN
      vga_vsync_n <= vga_vsync_n2;
      vga_blank_n <= vga_blank_n2;
      vga_hsync_n <= vga_hsync_n2;
      vga_vsync_n2 <= vga_vsync_n1;
      vga_blank_n2 <= vga_blank_n1;
      vga_hsync_n2 <= vga_hsync_n1;
    END IF;
  END PROCESS;
  pixelcounter: PROCESS(fpga_clk_65M)
  VARIABLE new_cnt : std_logic_vector (10 DOWNTO 0);
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      IF(reset_n = '0') THEN
        new_cnt := (others => '0');
      ELSE
        new_cnt := hcnt + 1;
        IF(new_cnt = 1344) THEN
          new_cnt := (others => '0');
        END IF;
      END IF;
      hcnt <= new_cnt;
    END IF;
  END PROCESS;
  linecounter: PROCESS(fpga_clk_65M)
    VARIABLE last_hsync : std_logic;
    VARIABLE new_cnt : std_logic_vector (9 DOWNTO 0);
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      IF(reset_n = '0') THEN
        new_cnt := (others => '0');
      ELSIF(last_hsync = '0' AND vga_hsync_n1 = '1') THEN
        new_cnt := vcnt + 1;
        IF(new_cnt = 806) THEN
          new_cnt := (others => '0');
        END IF;
      END IF;
      vcnt <= new_cnt;
      last_hsync := vga_hsync_n1;
    END IF;
  END PROCESS;
  hsyncr: PROCESS(fpga_clk_65M)
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      IF(reset_n = '0') THEN
        vga_hsync_n1 <= '1';
        hblank <= '1';
      ELSE
        IF(hcnt < 1024 + 24) THEN
          -- pixels then front porch
          vga_hsync_n1 <= '1';
        ELSIF(hcnt < 1024 + 24 + 136) THEN
          -- hsync
          vga_hsync_n1 <= '0';
        ELSE
          -- back porch
          vga_hsync_n1 <= '1';
        END IF;
        IF(hcnt < 1024) THEN
          -- pixels
          hblank <= '0';
        ELSE
          -- front porch then hsync then back porch
          hblank <= '1';
        END IF;
      END IF;
    END IF;
  END PROCESS;
  vsyncr: PROCESS(fpga_clk_65M)
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      IF(reset_n = '0') THEN
        vga_vsync_n1 <= '1';
        vblank <= '1';
      ELSE
        IF(vcnt < 768 + 3) THEN
          -- lines then front porch
          vga_vsync_n1 <= '1';
        ELSIF(vcnt < 768 + 3 + 6) THEN
          -- vsync
          vga_vsync_n1 <= '0';
        ELSE
          -- back porch
          vga_vsync_n1 <= '1';
        END IF;
        IF(vcnt < 768) THEN
          --lines
          vblank <= '0';
        ELSE
          -- front porch then vsync then back porch
          vblank <= '1';
        END IF;
      END IF;
    END IF;
  END PROCESS;
  blank_syncr: PROCESS(fpga_clk_65M)
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      vga_blank_n1 <= NOT (hblank OR vblank);
    END IF;
  END PROCESS;
  vga_output: PROCESS(fpga_clk_65M)
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      r_vga <= NOT (hblank OR vblank);
      IF(hcnt > 1000 AND vcnt > 700) THEN
        vga_r <= (others => '1');
        vga_g <= (others => '0');
        vga_b <= (others => '0');
      ELSE
        vga_r(7 DOWNTO 2) <= "10101";
        vga_r(1 DOWNTO 0) <= (others => '0');
        vga_g(7 DOWNTO 3) <= "1010";
        vga_g(2 DOWNTO 0) <= (others => '0');
        vga_b(7 DOWNTO 3) <= "1010";
        vga_b(2 DOWNTO 0) <= (others => '0');

--        vga_r(7 DOWNTO 2) <= data_out_graphics(15 DOWNTO 10);
--        vga_r(1 DOWNTO 0) <= (others => '0');
--        vga_g(7 DOWNTO 3) <= data_out_graphics(9 DOWNTO 5);
--        vga_g(2 DOWNTO 0) <= (others => '0');
--        vga_b(7 DOWNTO 3) <= data_out_graphics(4 DOWNTO 0);
--        vga_b(2 DOWNTO 0) <= (others => '0');
      END IF;
    END IF;
  END PROCESS;
END ARCHITECTURE behavior;

