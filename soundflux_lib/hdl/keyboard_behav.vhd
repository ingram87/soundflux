--
-- VHDL Architecture soundflux_lib.keyboard.behav
--
-- Created:
--          by - danle157.student (southfork-15.edu.isy.liu.se)
--          at - 08:11:53 09/26/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY keyboard IS
   PORT( 
      fpga_clk_65M    : IN     std_logic;
      kb_clk          : IN     std_logic;
      kb_data         : IN     std_logic;
      reset_n         : IN     std_logic;
      scancode_stable : OUT    std_logic;
      scancode        : OUT    std_logic_vector (7 DOWNTO 0)
   );

-- Declarations

END keyboard ;

--
ARCHITECTURE behav OF keyboard IS
  SIGNAL kb_data_shift : std_logic_vector(9 downto 0);
  SIGNAL kb_data_sync : std_logic;
  SIGNAL kb_clk_sync : std_logic;
  SIGNAL kb_clk_previous : std_logic;
  SIGNAL kb_clk_falling_edge_found : std_logic;
  SIGNAL num_bits_read : unsigned(3 downto 0);
BEGIN  
  sync: PROCESS(fpga_clk_65M)
  BEGIN
    IF rising_edge(fpga_clk_65M) THEN
      kb_data_sync <= kb_data;
      kb_clk_sync <= kb_clk;
    END IF;
  END PROCESS;
  
  detect_edge: PROCESS(fpga_clk_65M)
  BEGIN
    IF rising_edge(fpga_clk_65M) THEN
      IF kb_clk_previous = '1' AND kb_clk_sync = '0' THEN
        kb_clk_falling_edge_found <= '1';
      ELSE
        kb_clk_falling_edge_found <= '0';
      END IF;
      
      kb_clk_previous <= kb_clk_sync;
    END IF;
  END PROCESS;
  
  read_kb_data: PROCESS(fpga_clk_65M, reset_n)
  BEGIN
    IF rising_edge(fpga_clk_65M) THEN
      IF kb_clk_falling_edge_found = '1' THEN
        -- Shifts kb_data_sync in from left. 
        -- LSB (right-most) of kb_data_shift is shifted out.
        kb_data_shift <= kb_data_sync & kb_data_shift(9 downto 1);
        IF num_bits_read < 11 THEN
          num_bits_read <= num_bits_read + 1;
        ELSE
          num_bits_read <= "0001";
        END IF;
      END IF;
      
      scancode <= kb_data_shift(7 downto 0);
    END IF;
    --Async reset of num_bits_read
    IF reset_n = '0' THEN
      num_bits_read <= (others => '0');
    END IF;
  END PROCESS;
  
  detect_stable_scancode: PROCESS(fpga_clk_65M)
  BEGIN
    IF rising_edge(fpga_clk_65M) THEN
      IF num_bits_read = 11 THEN
        scancode_stable <= '1';
      ELSE
        scancode_stable <= '0';
      END IF;
    END IF;
  END PROCESS;
END ARCHITECTURE behav;

