--
-- VHDL Architecture soundflux_lib.DATA_REGISTER.behav
--
-- Created:
--          by - marer661.student (egypten-08.edu.isy.liu.se)
--          at - 13:48:04 09/25/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE ieee.numeric_std.all;

ENTITY DATA_REGISTER IS
   PORT( 
      fpga_clk_65M : IN     std_logic;
      reset_n      : IN     std_logic;
      data_in      : OUT    std_logic_vector (15 DOWNTO 0);
      data_out     : IN     std_logic_vector (15 DOWNTO 0);
      rw_sound     : IN     RW_TYPE;
      sram_data    : INOUT  std_logic_vector (15 DOWNTO 0)
   );

-- Declarations

END DATA_REGISTER ;

ARCHITECTURE behav OF DATA_REGISTER IS
BEGIN
  clocked_register : PROCESS(fpga_clk_65M, reset_n)
  BEGIN
    IF reset_n = '0' THEN
      data_in <= (others => '0');      
    ELSIF rising_edge(fpga_clk_65M) THEN
      CASE rw_sound IS
      WHEN READ =>
        data_in <= sram_data;
      WHEN OTHERS =>
        null;
      END CASE;
    END IF;
  END PROCESS;
  
  WITH RW_SOUND SELECT
  sram_data <= data_out WHEN WRITE,
               (others => 'Z') WHEN others;
  
  --sram_data <= data_out;    
END ARCHITECTURE behav;

