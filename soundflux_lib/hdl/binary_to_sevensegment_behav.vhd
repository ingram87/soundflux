--
-- VHDL Architecture soundflux_lib.binary_to_sevensegment.behav
--
-- Created:
--          by - danle157.student (asgard-12.edu.isy.liu.se)
--          at - 09:27:16 09/30/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.ALL;

ENTITY binary_to_sevensegment IS
-- Declarations
  PORT(
    data_in : IN unsigned(3 downto 0);
    seven_out : OUT std_logic_vector(6 downto 0)
  );
END binary_to_sevensegment ;

--
ARCHITECTURE behav OF binary_to_sevensegment IS
BEGIN
  WITH data_in SELECT
    seven_out <= "1000000" WHEN "0000",
    "1111001" WHEN "0001",
     "0100100" WHEN "0010",
     "0110000" WHEN "0011",
     "0011001" WHEN "0100",
     "0010010" WHEN "0101",
     "0000010" WHEN "0110",
     "1111000" WHEN "0111",
     "0000000" WHEN "1000",
     "0010000" WHEN "1001",
     "0001000" WHEN "1010",
     "0000011" WHEN "1011",
     "1000110" WHEN "1100",
     "0100001" WHEN "1101",
    "0000110" WHEN "1110",
    "0001110" WHEN "1111",
    "0111111" WHEN others;  
END ARCHITECTURE behav;

