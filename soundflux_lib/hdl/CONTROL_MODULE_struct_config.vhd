-- Generation properties:
--   Format              : hierarchical
--   Generic mappings    : exclude
--   Leaf-level entities : direct binding
--   Regular libraries   : use library name
--   View name           : include
--   
LIBRARY soundflux_lib;
CONFIGURATION CONTROL_MODULE_struct_config OF CONTROL_MODULE IS
   FOR struct
      FOR ALL : control
         USE ENTITY soundflux_lib.control(behav);
      END FOR;
      FOR ALL : keyboard
         USE ENTITY soundflux_lib.keyboard(behav);
      END FOR;
   END FOR;
END CONTROL_MODULE_struct_config;
