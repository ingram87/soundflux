--
-- VHDL Package Header soundflux_lib.datatypes
--
-- Created:
--          by - marer661.student (egypten-05.edu.isy.liu.se)
--          at - 14:27:15 09/24/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

PACKAGE datatypes IS
  TYPE RW_TYPE IS (IDLE, WRITE, READ);
  TYPE DELAY_RANGE IS RANGE 0 TO 2600;
  TYPE COMMAND IS (IDLE,
                   VOL_UP,
                   VOL_DOWN,
                   BALANCE_LEFT,
                   BALANCE_RIGHT,
                   ECO_VOL_UP,
                   ECO_VOL_DOWN, 
                   ECO_DELAY_INC, 
                   ECO_DELAY_DEC);
END datatypes;
