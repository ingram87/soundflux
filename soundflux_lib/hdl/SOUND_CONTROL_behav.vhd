--
-- VHDL Architecture soundflux_lib.SOUND_CONTROL.behav
--
-- Created:
--          by - marer661.student (ixtab.edu.isy.liu.se)
--          at - 14:25:43 09/26/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE ieee.numeric_std.all;

ENTITY SOUND_CONTROL IS
   PORT( 
      fpga_clk_65M : IN std_logic;
      aud_xck     : OUT    std_logic;
      aud_bclk    : INOUT  std_logic;
      aud_daclrck : INOUT  std_logic;
      aud_adclrck : INOUT  std_logic;
      aud_adcdat  : IN     std_logic;
      aud_dacdat  : OUT    std_logic
   );

-- Declarations

END SOUND_CONTROL ;

--
ARCHITECTURE behav OF SOUND_CONTROL IS
BEGIN
  xck_gen: PROCESS(fpga_clk_65M)
  VARIABLE count : integer range 0 to 5;
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      -- MCLK/XTI/aud_xck is generated here. It shall
      -- be 256 * fs, fs ~= 48 kHz, = 10.8 MHz
      -- That is once every ~5.3 fpga clocks. Let's
      -- make it slightly slower and switch at every 3
      -- clocks. That gives us 42.3 kHz which should be
      -- fine. fs = 42.3 kHz.
      IF(count = 5) THEN
        count := 0;
      ELSE
        count := count + 1;
      END IF;
      
      IF(count < 3) THEN
        aud_xck <= '0';
      ELSE
        aud_xck <= '1';
      END IF;
      -- bclk only needs to be fast enough that it can
      -- fit all bits from the samples from both
      -- channels in fs (42.3 kHz).
    END IF;
  END PROCESS;
  sound_process: PROCESS(fpga_clk_65M)
  BEGIN
    IF(rising_edge(fpga_clk_65M)) THEN
      -- Default settings used: i2s-format, 24 bits, right channel DAC data when daclrc low
    END IF;
  END PROCESS;
END ARCHITECTURE behav;

