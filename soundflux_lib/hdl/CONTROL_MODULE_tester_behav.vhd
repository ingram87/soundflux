--
-- VHDL Architecture soundflux_lib.CONTROL_MODULE_tester.behav
--
-- Created:
--          by - danle157.student (olympen-14.edu.isy.liu.se)
--          at - 10:22:01 09/27/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.ALL;

ENTITY CONTROL_MODULE_tester IS
   PORT( 
      balance      : IN     unsigned (3 DOWNTO 0);
      eco_delay    : IN     unsigned (3 DOWNTO 0);
      eco_vol      : IN     unsigned (3 DOWNTO 0);
      vol          : IN     unsigned (3 DOWNTO 0);
      fpga_clk_65M : OUT    std_logic := '0';
      kb_clk       : OUT    std_logic;
      kb_data      : OUT    std_logic;
      reset_n      : OUT    std_logic
   );

-- Declarations

END CONTROL_MODULE_tester ;

--
ARCHITECTURE behav OF CONTROL_MODULE_tester IS
  CONSTANT sys_clk_period : TIME := 15 ns; -- ca 65MHz
  CONSTANT kb_clk_period : TIME := 50 us; -- ca 20KHz
  
  -- "stop parity" & "scancode" & "start"
  CONSTANT SCANCODE_W : std_logic_vector(10 downto 0) := "10" & "00011101" & "0";
  
  CONSTANT BREAKCODE : std_logic_vector(10 downto 0) := "10" & "11110000" & "0";
BEGIN
  fpga_clk_65M <= NOT fpga_clk_65M after SYS_CLK_PERIOD/2;
  
  test: PROCESS
    VARIABLE volume : INTEGER;
  BEGIN
    kb_clk <= '1';
    reset_n <= '0';
    wait for sys_clk_period;
    reset_n <= '1';
    
    -- TEST 1: Test sending W to controller
    -- Expect volume to be incremented.
    volume := to_integer(vol);
    FOR ii IN 0 to 10 LOOP
      kb_data <= SCANCODE_W(ii);
      wait for kb_clk_period/2;
      kb_clk <= '0';
      wait for kb_clk_period/2;
      kb_clk <= '1';
    END LOOP;
    wait for kb_clk_period;
    ASSERT to_integer(vol) = volume + 1
    REPORT "Test 1 failed."
    SEVERITY FAILURE;
    
    -- TEST 2: Test sending break then W to controller.
    -- Expect volume to remain the same.
    volume := to_integer(vol);
    FOR ii IN 0 to 10 LOOP
      kb_data <= BREAKCODE(ii);
      wait for kb_clk_period/2;
      kb_clk <= '0';
      wait for kb_clk_period/2;
      kb_clk <= '1';
    END LOOP;
    wait for kb_clk_period;
    FOR ii IN 0 to 10 LOOP
      kb_data <= SCANCODE_W(ii);
      wait for kb_clk_period/2;
      kb_clk <= '0';
      wait for kb_clk_period/2;
      kb_clk <= '1';
    END LOOP;
    ASSERT to_integer(vol) = volume
    REPORT "Test 2 failed."
    SEVERITY FAILURE;
    
    
    wait for 3*kb_clk_period;
    
    ASSERT FALSE
    REPORT "NONE: Test run finished"
    SEVERITY FAILURE;
  END PROCESS;
END ARCHITECTURE behav;

