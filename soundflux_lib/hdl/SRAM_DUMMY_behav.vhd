--
-- VHDL Architecture soundflux_lib.SRAM_DUMMY.behav
--
-- Created:
--          by - marer661.student (asgard-10.edu.isy.liu.se)
--          at - 09:01:47 09/30/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE ieee.numeric_std.all;

ENTITY SRAM_DUMMY IS
   PORT( 
      sram_data    : INOUT  std_logic_vector (15 DOWNTO 0);
      rw_sound     : IN     RW_TYPE;
      sram_address : IN     std_logic_vector (19 DOWNTO 0)
   );

-- Declarations

END SRAM_DUMMY ;

--
ARCHITECTURE behav OF SRAM_DUMMY IS
BEGIN  
  WITH RW_SOUND SELECT
  sram_data <= sram_address(15 DOWNTO 0) after 4 ns WHEN READ,
               (others => 'Z') WHEN others;
END ARCHITECTURE behav;

