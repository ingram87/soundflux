--
-- VHDL Architecture soundflux_lib.CODEC_CONTROL.behav
--
-- Created:
--          by - marer661.student (ixtab.edu.isy.liu.se)
--          at - 14:11:56 09/26/13
--
-- using Mentor Graphics HDL Designer(TM) 2012.2 (Build 11)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY soundflux_lib;
USE soundflux_lib.datatypes.all;
USE ieee.numeric_std.all;

ENTITY CODEC_CONTROL IS
   PORT( 
      fpga_clk_65M : IN     std_logic;
      reset_n      : IN     std_logic;
      sclk         : OUT    std_logic;
      sdat         : INOUT  std_logic
   );

-- Declarations

END CODEC_CONTROL ;

--
ARCHITECTURE behav OF CODEC_CONTROL IS
    TYPE state_type IS (IDLE, SOT, ADDR_RW, DATA_MSB, DATA_LSB, EOT);
    TYPE busy_state_type IS (IDLE, NOT_BUSY, BUSY); 
    TYPE config_data_type IS ARRAY (0 to 3) OF std_logic_vector(15 DOWNTO 0);
    
    SIGNAL state : state_type;
    SIGNAL busy_state : busy_state_type;
    SIGNAL config_data : std_logic_vector(15 DOWNTO 0);
    
    CONSTANT address_and_rw : std_logic_vector(7 DOWNTO 0) := "00110100";
    CONSTANT config_data_array : config_data_type := (("0000110-001100010"),   -- Power-up
                                                      ("0000000-000010111"), -- unmute left line in
                                                      ("0001001-000000001"), -- activate
                                                      ("----------------"));  -- Placeholder
  
    -- I2C clock can not be higher than 12.8 MHz
    SIGNAL i2c_count : integer range 0 to 11;
BEGIN
    
    forward_clk : PROCESS(fpga_clk_65M)
    BEGIN
      IF(rising_edge(fpga_clk_65M)) THEN
        IF NOT (state = IDLE) THEN
          IF(i2c_count < 6) THEN
            sclk <= '0';
          ELSE
            sclk <= '1';
          END IF;
          i2c_count <= i2c_count + 1;
        ELSE
          sclk <= '1';
        END IF;
      END IF;
    END PROCESS;
    
    setup_codec : PROCESS(fpga_clk_65M, reset_n)
      TYPE config_data_counter_type IS RANGE config_data_array'RANGE;
      
      VARIABLE data : std_logic_vector(7 DOWNTO 0);
      VARIABLE data_counter : NATURAL;
      VARIABLE config_data_counter : NATURAL;
      
    BEGIN
      IF reset_n = '0' THEN
        
        -- Reset to IDLE state
        state <= IDLE;
        sdat <= '1';
        config_data_counter := 0;
        busy_state <= NOT_BUSY;
        
      ELSIF rising_edge(fpga_clk_65M) AND (i2c_count = 4) THEN
        CASE busy_state IS
        WHEN NOT_BUSY =>
          
          sdat <= '1';
          
          IF config_data_counter = config_data_type'HIGH + 1 THEN
            -- Stop counting and assume IDLE state
            busy_state <= IDLE;      
          ELSE          
            -- Update contents of config_data and start the transmission
            config_data <= config_data_array(config_data_counter);
            busy_state <= BUSY;
          END IF;
          
          -- Increment counter
          config_data_counter := config_data_counter + 1; 
          
        WHEN BUSY =>
          
          CASE state IS
          WHEN IDLE =>
            
            --IF reset_codec = '1' THEN
            
            -- Initiate start of transmission
            sdat <= '0';
            state <= ADDR_RW;
            data := address_and_rw;
            data_counter := 8;
            
            --END IF;
            
          WHEN ADDR_RW =>
            
            -- Send 
            IF data_counter > 0 THEN        
              data_counter := data_counter - 1;                
              sdat <= data(data_counter);        
            ELSE
              sdat <= 'Z';
              -- Possible wait for ack.          
              
              state <= DATA_MSB;
              data := config_data(15 DOWNTO 8);
              data_counter := 8;
            END IF;
            
          WHEN DATA_MSB =>
            
            -- Send 8 MSB
            IF data_counter > 0 THEN        
              data_counter := data_counter - 1;                
              sdat <= data(data_counter);        
            ELSE
              sdat <= 'Z';
              -- Possible wait for ack.          
              
              state <= DATA_LSB;
              data := config_data(7 DOWNTO 0);
              data_counter := 8;
            END IF;
            
          WHEN DATA_LSB =>
            
            -- Send 
            IF data_counter > 0 THEN        
              data_counter := data_counter - 1;                
              sdat <= data(data_counter);        
            ELSE
              sdat <= 'Z';
              -- Possible wait for ack.          
              
              state <= EOT;
              data := (others => '0');
            END IF;
            
          WHEN EOT =>
            -- End the transmission
            state <= IDLE;
            sdat <= '0';
            busy_state <= NOT_BUSY;
          END CASE;
          
        WHEN IDLE => 
          
          sdat <= '1';
          
        END CASE;
      END IF;
    END PROCESS;
    
  END ARCHITECTURE behav;
  
  
  
  
